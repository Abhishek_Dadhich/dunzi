package org.processor;

import Processor.App;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import java.util.ArrayList;

public class BeverageTest {
    ArrayList<Object> testList ;
    App app;

    @Before
    public void setUp(){
        testList = new ArrayList<Object>();
        testList.add(0);
        testList.add(1);
        testList.add(2);
        testList.add(3);


        app = new App();
    }

    @Test
    public void testChaiPoint(){
        int result=app.exploreChaiPoint(testList, 4);
        Assert.assertEquals(1,result);
    }
}
