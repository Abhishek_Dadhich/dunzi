package Beverage;

import Configs.PARAMS;
import Ingredients.HotMilk;
import Ingredients.HotWater;

import java.util.HashMap;

public class Water extends Beverage{
    public Water() {
        name = "hot_Water";
        howToMake();
    }

    protected void howToMake(){
        ingredientRatio = new HashMap<String, Integer>();
        ingredientRatio.put(HotWater.getInstance(PARAMS.HotWater).getName(), 50);
        ingredints.add(HotWater.getInstance(PARAMS.HotWater));
    }
}
