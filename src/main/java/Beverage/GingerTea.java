package Beverage;

import Configs.PARAMS;
import Ingredients.GreenMixture;
import Ingredients.Gynger;

public class GingerTea extends Tea {

    public GingerTea() {
        name = "hot_tea";
        howToMake();
    }
    @Override
    protected void howToMake() {
        super.howToMake();
        ingredientRatio.put(Gynger.getInstance(PARAMS.Gynger).getName(), 10);
        ingredints.add(Gynger.getInstance());
    }
}
