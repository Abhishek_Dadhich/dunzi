package Beverage;

import Configs.PARAMS;
import Ingredients.GreenMixture;
import Ingredients.HotMilk;

import java.util.HashMap;

public class Milk extends Beverage{

    public Milk() {
        name = "hot_Milk";
        howToMake();
    }

    protected void howToMake(){
        ingredientRatio = new HashMap<String, Integer>();
        ingredientRatio.put(HotMilk.getInstance(PARAMS.HotMilk).getName(), 50);
        ingredints.add(HotMilk.getInstance(PARAMS.HotMilk));
    }
}
