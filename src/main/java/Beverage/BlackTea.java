package Beverage;

import Configs.PARAMS;
import Ingredients.*;

import java.util.HashMap;

public class BlackTea extends Beverage{
    public BlackTea() {
        name = "black_tea";
        howToMake();
    }

    private void howToMake(){
        ingredientRatio = new HashMap<String, Integer>();
        ingredientRatio.put(Sugar.getInstance(PARAMS.sugar).getName(), 50);
        ingredientRatio.put(HotWater.getInstance(PARAMS.HotWater).getName(), 300);
        ingredientRatio.put(Gynger.getInstance(PARAMS.Gynger).getName(), 30);
        ingredientRatio.put(TeaLeaves.getInstance(PARAMS.TeaLeaves).getName(), 30);



        ingredints.add(Sugar.getInstance(PARAMS.sugar));
        ingredints.add(HotWater.getInstance(PARAMS.HotWater));
        ingredints.add(Gynger.getInstance(PARAMS.Gynger));
        ingredints.add(TeaLeaves.getInstance());



    }
}
