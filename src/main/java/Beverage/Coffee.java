package Beverage;

import Configs.PARAMS;
import Ingredients.*;

import java.util.HashMap;
import java.util.Map;

public class Coffee extends Beverage{

    public Coffee() {
        name = "hot_coffee";
        howToMake();
    }

    private void howToMake(){
        ingredientRatio = new HashMap<String, Integer>();
        ingredientRatio.put(Sugar.getInstance(PARAMS.sugar).getName(), 50);
        ingredientRatio.put(HotWater.getInstance(PARAMS.HotWater).getName(), 100);
        ingredientRatio.put(HotMilk.getInstance(PARAMS.HotMilk).getName(), 400);
        ingredientRatio.put(TeaLeaves.getInstance(PARAMS.TeaLeaves).getName(), 30);
        ingredientRatio.put(Gynger.getInstance(PARAMS.Gynger).getName(), 30);


        ingredints.add(Sugar.getInstance(PARAMS.sugar));
        ingredints.add(HotWater.getInstance(PARAMS.HotWater));
        ingredints.add(HotMilk.getInstance(PARAMS.HotMilk));
        ingredints.add(Gynger.getInstance(PARAMS.Gynger));
        ingredints.add(TeaLeaves.getInstance(PARAMS.TeaLeaves));

    }

}
