package Beverage;

import Ingredients.Ingredient;

import java.util.ArrayList;
import java.util.Map;
import java.util.logging.Logger;

public abstract class Beverage implements Runnable{
    ArrayList<Ingredient> ingredints = new ArrayList<>();
    Map<String, Integer> ingredientRatio;
    static Logger log = Logger.getLogger(Beverage.class.getName());

    public String getName() {
        return name;
    }

    String name;

    public int cost(){
        int sum = 0;
        for (Ingredient i: ingredints){
            sum += i.cost();
        }
        return sum;
    }
    public boolean canWeMake(){
        for (Ingredient i : ingredints){
            if (i.getQuantity() < ingredientRatio.get(i.getName())) {
                if (i.isAvailable()){
                    log.info(i.getName() + "is not sufficient");
                }
                else{
                    log.info(i.getName() + "is not available");
                }
                return false;
            }
        }
        return true;
    }

    public int serve(){
        String response = name + " is prepared";
        int c = 0;
        try {
            for (Ingredient i : ingredints) {

                int used = ingredientRatio.get(i.getName());
                i.add(-1 * used);
            }
            c = cost();
        }
        catch (Exception e){
            response = e.getMessage();
        }
        finally {
            log.info(response);
            return c;
        }
    }
    @Override
    public void run() {
        serve();
    }
}
