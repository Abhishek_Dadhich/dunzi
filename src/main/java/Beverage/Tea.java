package Beverage;


import Configs.PARAMS;
import Ingredients.*;

import java.util.HashMap;

public abstract class Tea extends Beverage{

    protected void howToMake(){
        ingredientRatio = new HashMap<String, Integer>();
        ingredientRatio.put(Sugar.getInstance(PARAMS.sugar).getName(), 10);
        ingredientRatio.put(HotWater.getInstance(PARAMS.HotWater).getName(), 200);
        ingredientRatio.put(HotMilk.getInstance(PARAMS.HotMilk).getName(), 100);
        ingredientRatio.put(TeaLeaves.getInstance(PARAMS.TeaLeaves).getName(), 30);


        ingredints.add(Sugar.getInstance(PARAMS.sugar));
        ingredints.add(HotWater.getInstance(PARAMS.HotWater));
        ingredints.add(HotMilk.getInstance(PARAMS.HotMilk));
        ingredints.add(TeaLeaves.getInstance());
    }


}
