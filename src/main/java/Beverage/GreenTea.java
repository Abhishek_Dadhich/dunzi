package Beverage;

import Configs.PARAMS;
import Ingredients.*;

import java.util.HashMap;

public class GreenTea extends Beverage{
    public GreenTea() {
        name = "green_tea";
        howToMake();
    }

    private void howToMake(){
        ingredientRatio = new HashMap<String, Integer>();
        ingredientRatio.put(Sugar.getInstance(PARAMS.sugar).getName(), 50);
        ingredientRatio.put(HotWater.getInstance(PARAMS.HotWater).getName(), 100);
        ingredientRatio.put(Gynger.getInstance(PARAMS.Gynger).getName(), 30);
        ingredientRatio.put(GreenMixture.getInstance(PARAMS.GreenMixture).getName(), 30);


        ingredints.add(Sugar.getInstance(PARAMS.sugar));
        ingredints.add(HotWater.getInstance(PARAMS.HotWater));
        ingredints.add(Gynger.getInstance(PARAMS.Gynger));
        ingredints.add(GreenMixture.getInstance());


    }
}
