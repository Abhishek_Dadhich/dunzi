package Processor;

import Beverage.Beverage;
import Beverage.Coffee;
import Beverage.GingerTea;
import Beverage.Milk;
import Beverage.Water;
import Beverage.GreenTea;
import Beverage.GreenTea;
import Beverage.BlackTea;
import Ingredients.*;


import java.util.ArrayList;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

public class ChaiPoint{
    int capacity = 0;
    ArrayList<Beverage> offerings;

    String name = "Chai Point";
    ExecutorService executorService;
    StockCheck observer = new StockCheck();

    public ChaiPoint(int N){
        capacity  = N;
        offerings = new ArrayList<Beverage>(5);
        offerings.add(new GingerTea());
        offerings.add(new Coffee());
        offerings.add(new BlackTea());
        offerings.add(new GreenTea());

        executorService = Executors.newFixedThreadPool(N);

        CoffeeSyrup.getInstance().addListener(observer);
        GreenMixture.getInstance().addListener(observer);
        Gynger.getInstance().addListener(observer);
        HotMilk.getInstance().addListener(observer);
        HotWater.getInstance().addListener(observer);
        Sugar.getInstance().addListener(observer);
        TeaLeaves.getInstance().addListener(observer);

    }
    public int chooseBeverage(Object i){
        Beverage b = offerings.get((int)i);
        executorService.execute(b);
        return 200;
    }
}
