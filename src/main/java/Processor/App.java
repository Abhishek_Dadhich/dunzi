package Processor;

import java.util.ArrayList;


public class App {
    public static void main(String[] args){
        ChaiPoint chaiPoint = new ChaiPoint(2);

        ArrayList customers = new ArrayList(5);
        customers.add(0);
        customers.add(1);
        customers.add(2);
        customers.add(3);

        for(Object customer : customers){
            chaiPoint.chooseBeverage(customer);
        }
        chaiPoint.executorService.shutdown();
    }
    public int exploreChaiPoint(ArrayList<Object> customers, int n){
        ChaiPoint chaiPoint = new ChaiPoint(n);
        for(Object customer : customers){
            chaiPoint.chooseBeverage(customer);
        }
        chaiPoint.executorService.shutdown();
        return 1;
    }
}
