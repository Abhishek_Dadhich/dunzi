package Processor;

import Beverage.Beverage;
import Configs.MinStock;
import Ingredients.Ingredient;
import javafx.beans.InvalidationListener;

import java.util.Observable;
import java.util.Observer;
import java.util.logging.Logger;

public class StockCheck implements InvalidationListener {
    static Logger log = Logger.getLogger(Beverage.class.getName());


    @Override
    public void invalidated(javafx.beans.Observable observable) {
        Ingredient ingredient = (Ingredient)observable;
        if (ingredient.getQuantity() < ingredient.getMin_value()){
            log.warning(ingredient.getName() + " is about to finish. Please refill");
        }

    }
}
