package Ingredients;

import Configs.MinStock;

public class GreenMixture extends Ingredient{

    static Ingredient ingredient = null;

    public static Ingredient getInstance(){
        if (ingredient == null){
            ingredient = new GreenMixture();
        }
        return ingredient;
    }
    public static Ingredient getInstance(int quantity){
        if (ingredient == null){
            ingredient = new GreenMixture(quantity);
        }
        return ingredient;
    }

    private GreenMixture(int quantity) {
        this.quantity = quantity;
        name = "GreenMixture";
        min_value = MinStock.GreenMixture;
        log.info(this.name + " stock:" + this.getQuantity());
    }
    private GreenMixture() {
        this.quantity = 0;
        name = "GreenMixture";
    }
    @Override
    public int cost() {
        return 0;
    }
}
