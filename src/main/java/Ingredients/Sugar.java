package Ingredients;

import Configs.MinStock;

public class Sugar extends Ingredient{

    static Ingredient ingredient = null;

    public static Ingredient getInstance(){
        if (ingredient == null){
            ingredient = new Sugar();
        }
        return ingredient;
    }
    public static Ingredient getInstance(int quantity){
        if (ingredient == null){
            ingredient = new Sugar(quantity);
        }
        return ingredient;
    }

    private Sugar(int quantity) {
        this.quantity = quantity;
        name = "Sugar";
        log.info(this.name + " stock:" + this.getQuantity());
    }
    private Sugar() {
        this.quantity = 0;
        name = "Sugar";
        min_value = MinStock.sugar;
        log.info(this.name + " stock:" + this.getQuantity());
    }

    @Override
    public int cost() {
        return 0;
    }
}
