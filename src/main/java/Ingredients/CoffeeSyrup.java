package Ingredients;

import Configs.MinStock;

public class CoffeeSyrup extends Ingredient{

    static Ingredient ingredient = null;

    public static Ingredient getInstance(){
        if (ingredient == null){
            ingredient = new CoffeeSyrup();
        }
        return ingredient;
    }
    public static Ingredient getInstance(int quantity){
        if (ingredient == null){
            ingredient = new CoffeeSyrup(quantity);
        }
        return ingredient;
    }

    private CoffeeSyrup(int quantity) {
        this.quantity = quantity;
        min_value = MinStock.CoffeeSyrup;
        name = "CoffeeSyrup";
    }
    private CoffeeSyrup() {
        this.quantity = 0;
        name = "CoffeeSyrup";
    }

    @Override
    public int cost() {
        return 0;
    }
}
