package Ingredients;

import Configs.MinStock;

public class Gynger extends Ingredient{

    static Ingredient ingredient = null;

    public static Ingredient getInstance(){
        if (ingredient == null){
            ingredient = new Gynger();
        }
        return ingredient;
    }
    public static Ingredient getInstance(int quantity){
        if (ingredient == null){
            ingredient = new Gynger(quantity);
        }
        return ingredient;
    }

    private Gynger(int quantity) {
        this.quantity = quantity;
        name = "Gynger";
        min_value = MinStock.Gynger;
        log.info(this.name + " stock:" + this.getQuantity());
    }
    private Gynger() {
        this.quantity = 0;
        name = "Gynger";
    }
    @Override
    public int cost() {
        return 0;
    }
}
