package Ingredients;

import Configs.MinStock;

public class TeaLeaves extends Ingredient{

    static Ingredient ingredient = null;

    public static Ingredient getInstance(){
        if (ingredient == null){
            ingredient = new TeaLeaves();
        }
        return ingredient;
    }
    public static Ingredient getInstance(int quantity){
        if (ingredient == null){
            ingredient = new TeaLeaves(quantity);
        }
        return ingredient;
    }

    private TeaLeaves(int quantity) {
        this.quantity = quantity;
        name = "CoffeeSyrup";
        min_value = MinStock.TeaLeaves;
        log.info(this.name + " stock:" + this.getQuantity());
    }
    private TeaLeaves() {
        this.quantity = 0;
        name = "CoffeeSyrup";
    }
    @Override
    public int cost() {
        return 0;
    }
}
