package Ingredients;

import Configs.MinStock;
import javafx.beans.InvalidationListener;

public class HotWater extends Ingredient {

    static Ingredient ingredient = null;

    public static Ingredient getInstance(){
        if (ingredient == null){
            ingredient = new HotWater();
        }
        return ingredient;
    }
    public static Ingredient getInstance(int quantity){
        if (ingredient == null){
            ingredient = new HotWater(quantity);
        }
        return ingredient;
    }

    private HotWater(int quantity) {
        this.quantity = quantity;
        name = "HotWater";
        min_value = MinStock.HotWater;
        log.info(this.name + " stock:" + this.getQuantity());
    }
    private HotWater() {
        this.quantity = 0;
        name = "HotWater";
    }

    @Override
    public int cost() {
        return 0;
    }

}
