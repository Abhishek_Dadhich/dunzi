package Ingredients;

import Configs.MinStock;

public class HotMilk extends Ingredient{

    static Ingredient ingredient = null;

    public static Ingredient getInstance(){
        if (ingredient == null){
            ingredient = new HotMilk();
        }
        return ingredient;
    }
    public static Ingredient getInstance(int quantity){
        if (ingredient == null){
            ingredient = new HotMilk(quantity);
        }
        return ingredient;
    }

    private HotMilk(int quantity) {
        this.quantity = quantity;
        name = "HotMilk";
        min_value = MinStock.HotMilk;
        log.info(this.name + " stock:" + this.getQuantity());
    }
    private HotMilk() {
        this.quantity = 0;
        name = "HotMilk";
    }
    @Override
    public int cost() {
        return 0;
    }
}
