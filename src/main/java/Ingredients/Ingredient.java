package Ingredients;

import Configs.MinStock;
import Configs.PARAMS;
import Processor.StockCheck;
import com.sun.source.tree.SynchronizedTree;
import javafx.beans.InvalidationListener;
import javafx.beans.Observable;



import java.nio.charset.MalformedInputException;
import java.util.ArrayList;
import java.util.logging.Logger;

public abstract class Ingredient implements Observable {

    String name = "";
    int quantity = 100000;
    ArrayList<StockCheck> listeners = new ArrayList<>();
    static Logger log = Logger.getLogger(Ingredient.class.getName());

    public int getMin_value() {
        return min_value;
    }

    int min_value;

    public abstract int cost() ;

    public String getName() {
        return name;
    }

    public synchronized void add(int quantity) throws Exception {
        if (-1*quantity > this.quantity){
            if (this.quantity == 0){
                throw new Exception(name + " is not available");
            }
            else{
                throw new Exception(name + " is not sufficient");
            }
        }
        this.quantity += quantity;
        for(StockCheck sc: listeners){
            sc.invalidated(this);
        }
    };
    public int getQuantity(){ return this.quantity; };
    public boolean isAvailable(){ return quantity == 0; }


    @Override
    public void addListener(InvalidationListener listener) {
        listeners.add((StockCheck) listener);
    }

    @Override
    public void removeListener(InvalidationListener listener) {
        listeners.remove((StockCheck) listener);
    }
}
